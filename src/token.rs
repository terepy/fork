use crate::*;
use serde::{Serialize,Deserialize};

#[derive(Debug, Copy, Clone, Eq, PartialEq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Token {
    Arg(u16),
    Literal(i32),
    Neg,
    Add,
    Mul,
    Call(Id),
}
pub use Token::*;

impl Token {
    pub fn inputs(self, db: &Db) -> u16 {
        match self {
            Arg(_) | Literal(_) => 0,
            Neg => 1,
            Add | Mul => 2,
            Call(id) => db.get(id).expr.args,
        }
    }
    
    pub fn outputs(self, db: &Db) -> u16 {
        match self {
            Call(id) => db.get(id).expr.outputs,
            _ => 1,
        }
    }

    pub fn precedence(self) -> usize {
        match self {
            Arg(_) | Literal(_) => 4,
            Neg => 3,
            Add => 2,
            Mul => 1,
            Call(_) => 0,
        }
    }

    pub fn val(self, args: &[Value], mut stack: Vec<Value>, db: &Db) -> Vec<Value> {
        match self {
            Arg(n) => stack.push(args[n as usize].clone()),
            Literal(n) => stack.push(Integer(n)),
            Neg => match stack.pop().unwrap() {
                Integer(n) => stack.push(Integer(-n)),
                _ => panic!("type mismatch"),
            },
            Add => {
                let right = stack.pop().unwrap();
                let left = stack.pop().unwrap();
                match (left, right) {
                    (Integer(l), Integer(r)) => stack.push(Integer(l + r)),
                    _ => panic!("type mismatch"),
                }
            }
            Mul => {
                let right = stack.pop().unwrap();
                let left = stack.pop().unwrap();
                match (left, right) {
                    (Integer(l), Integer(r)) => stack.push(Integer(l * r)),
                    _ => panic!("type mismatch"),
                }
            }
            Call(id) => {
                let expr = &db.get(id).expr;
                let args: Vec<_> = stack.drain((stack.len() - expr.args as usize)..).collect();
                stack.extend(expr.val(&args, db));
            }
        }
        stack
    }

    pub fn render(self, expr: &mut SubExpr, out: &mut String, db: &Db) {
        match self {
            Arg(n) => { //TODO: arg names
                //*arg_num -= 1;
                //out.extend(arg_name(*arg_num).chars().rev());
                out.extend(format!("${n}").chars().rev());
            }
            Literal(n) => out.extend(format!("{n}").chars().rev()),
            Neg => {
                let child = expr.pop();
                if child.precedence() < self.precedence() { out.push(')'); }
                child.render(expr, out, db);
                if child.precedence() < self.precedence() { out.push('('); }
                out.push('-');
            }
            Add | Mul => {
                let right = expr.pop();
                if right.precedence() <= self.precedence() { out.push(')'); }
                right.render(expr, out, db);
                if right.precedence() <= self.precedence() { out.push('('); }

                let op = if self == Add { " + " } else { " * " };
                *out += op;
                
                let left = expr.pop();
                if left.precedence() < self.precedence() { out.push(')'); }
                left.render(expr, out, db);
                if left.precedence() < self.precedence() { out.push('('); }
            }
            Call(id) => {
                /*let hex = |&n| [n >> 4, n & 0xF].map(|n| (n + b'0') as char);
                let name: String = id.iter().flat_map(hex).collect(); //TODO: fetch actual name*/
                let entry = db.get(id);
                let func = &entry.expr;
                let default = String::new();
                let name = entry.names.last().unwrap_or(&default);
                
                out.extend(name.chars().rev());
                
                for i in (0..func.args).rev() {
                    let arg = expr.pop();
                    out.push(' ');
                    if i + 1 != func.args {
                        out.push(',');
                    }
                    arg.render(expr, out, db);
                }
            }
        }
    }
}
