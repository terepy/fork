use std::sync::mpsc::{channel, Sender, SendError};
use std::thread;

use decorum::Finite;

use crate::*;

#[derive(Clone,Eq,PartialEq,Ord,PartialOrd,Hash)]
struct Node {
    token: Token,
    score: u16, //lower is better
    best_descendent_score: u16,
    descendents: usize,
    children: Vec<Node>,
}

impl Node {
    fn search(&mut self, state: &mut SearchState, send: &Sender<ScoredExpr>, parent_score: u16, mut budget: usize) -> Result<(), SendError<ScoredExpr>> {
        let first_explo = self.children.is_empty();
        if first_explo {
            budget -= 1;
            macro try_token($token: expr) {{
                if $token.inputs(&state.db) > state.current.outputs { continue; }
                if $token == Neg && self.token == Neg { continue; }
                let score = match state.push($token) {
                    Ok(s) => s,
                    Err(()) => continue,
                };
                let child = Node {
                    token: $token,
                    score,
                    best_descendent_score: u16::max_value(),
                    descendents: 0,
                    children: Vec::new(),
                };
                state.pop();
                self.best_descendent_score = self.best_descendent_score.min(score);
                self.children.push(child);
            }}
            for t in [Neg,Add,Mul] {
                try_token!(t);
            }
            //TODO: do we want the ability to generate expressions of the form $1 + $0?
            for i in 0..=state.current.args {
                try_token!(Arg(i));
            }
            for i in (-8)..=7 {
                try_token!(Literal(i));
            }
            let funcs: Vec<_> = state.scope.iter().map(|(_name, &id)| Call(id)).collect();
            for t in funcs {
                try_token!(t);
            }
            if self.score <= self.best_descendent_score.min(parent_score) {
                send.send(ScoredExpr { score: self.score, expr: state.current.clone() })?;
            }
            self.descendents = self.children.len();
            if budget == 0 { return Ok(()); }
        }
        let mut potentials = Vec::with_capacity(self.children.len());
        let mut total = 0.0;
        for i in 0..self.children.len() {
            let p = self.children[i].potential();
            potentials.push((Finite::from(p*p), i));
            total += p*p;
        }
        potentials.sort();
        let mut budgets = vec![(0, 0); potentials.len()];
        for i in 0..budgets.len() {
            let amt = ((potentials[i].0.into_inner() / total * budget as f32).floor() as usize).min(budget);
            budgets[i] = (amt, potentials[i].1);
            budget -= amt;
            if budget > 0 { budget -= 1; budgets[i].0 += 1; }
        }
        budgets[0].0 += budget;
        for (budget, i) in budgets {
            if budget == 0 { continue; }
            let child = &mut self.children[i];
            let old = child.descendents;
            state.push(child.token).unwrap();
            child.search(state, send, self.score, budget)?;
            state.pop();
            self.best_descendent_score = self.best_descendent_score.min(child.best_descendent_score);
            self.descendents += child.descendents - old;
        }
        Ok(())
    }

    fn potential(&self) -> f32 {
        1.0 / (1.0 //a node seems promising if:
            + self.best_descendent_score.min(self.score) as f32 //1 it has a good score somewhere in its descendents
            + (self.descendents as f32).sqrt() //2 it has few descendents
        )
    }
    
    /*fn prune_something(&mut self) -> usize {
        if self.children.iter().all(|x| x.children.is_empty()) {
            //base case
            self.children.clear();
            self.pruned = true;
            let num_pruned = self.descendents + 1;
            self.descendents = 0;
            num_pruned
        } else {
            self.children.sort_by_key(|x| x.score.min(x.best_descendent_score));
            for node in self.children.iter_mut().rev() {
                if node.children.is_empty() { continue; }
                let r = node.prune_something();
                self.descendents -= r;
                if r != 0 {
                    return r;
                }
            }
            0
        }
    }*/
}

struct SearchState {
    goal: Expr,
    current: Expr,
    mismatch_penalty: u16,
    gap_penalty: u16,
    scope: HashMap<String, Id>,
    db: Db,
    similarity: Vec<Vec<u16>>, //the last line of needleman wunsch, can be easily extended with another token
}

impl SearchState {
    fn new(goal: Expr, mismatch_penalty: u16, gap_penalty: u16, scope: HashMap<String, Id>, db: Db) -> Self {
        let similarity = vec![(0..=goal.tokens.len()).map(|i| i as u16 * gap_penalty).collect()];
        Self {
            goal,
            current: Expr::default(),
            mismatch_penalty,
            gap_penalty,
            scope,
            db,
            similarity,
        }
    }
    
    fn push(&mut self, token: Token) -> Result<u16, ()> { //TODO: change push/pop behaviour for repeat literals
        //TODO: optimization to eliminate this clone
        self.current = self.current.clone().push(token, &self.db).map_err(|_| ())?;
        let token = *self.current.tokens.last().unwrap();
        
        let old_sim = self.similarity.last().unwrap();
        let mut new_sim = vec![0; old_sim.len()];
        new_sim[0] = old_sim[0] + self.gap_penalty;
        for i in 1..=self.goal.tokens.len() {
            let up = old_sim[i] + self.gap_penalty;
            let upleft = old_sim[i-1] + (self.goal.tokens[i-1] != token) as u16 * self.mismatch_penalty;
            let left = new_sim[i-1] + self.gap_penalty;
            new_sim[i] = up.min(upleft).min(left);
        }
        self.similarity.push(new_sim);
        
        let score = *self.similarity.last().unwrap().last().unwrap();
        Ok(score)
    }

    fn pop(&mut self) {
        self.similarity.pop();
        self.current = self.current.clone().pop(&self.db);
    }
}

//TODO: we should consider switching from channels to mutexes, would simplify it a lot
pub fn start_search_loop() -> Sender<(Expr, HashMap<String, Id>, Db, Sender<ScoredExpr>)> {
    let (send, recv) = channel();
    thread::spawn(move|| {
        let mut root = Node {
            token: Arg(0), //the root's token is ignored, we can just put any dummy
            score: u16::max_value(),
            best_descendent_score: u16::max_value(),
            descendents: 0,
            children: Vec::new(),
        };
        let (goal, scope, db, mut send) = recv.recv().unwrap();
        let mut state = SearchState::new(goal, 2, 1, scope, db);
        let search_budget = 1 << 16;
        loop {
            let error = root.search(&mut state, &send, root.score, search_budget).is_err();
            /*while root.descendents > (1 << 16) {
                let r = root.prune_something();
                if r == 0 {
                    println!("warning: couldn't finish pruning");
                    break
                }
            }*/
            let mut new = None;
            for n in recv.try_iter() { //clear out the channel
                new = Some(n);
            }
            if new.is_none() && error { //block on new goal
                new = recv.recv().ok();
            }
            if let Some((goal, scope, db, s)) = new {
                send = s;
                state = SearchState::new(goal, 2, 1, scope, db);
                //TODO: migrate tree
                root = Node {
                    token: Arg(0),
                    score: u16::max_value(),
                    best_descendent_score: u16::max_value(),
                    descendents: 0,
                    children: Vec::new(),
                }
            }
        }
    });
    send
}

#[derive(Default,Debug,Clone)]
pub struct ScoredExpr {
    pub score: u16,
    pub expr: Expr,
}
