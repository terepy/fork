use std::collections::HashMap;
use crate::{Expr,Id};

#[derive(Debug,Clone)]
pub struct Entry {
    pub id: Id,
    pub expr: Expr,
    pub names: Vec<String>,
}

#[derive(Default,Clone)]
pub struct Db {
    entries: HashMap<Id, Entry>,
}

impl Db {
    pub fn add(&mut self, expr: Expr) -> Id {
        let id = expr.id();
        if self.entries.contains_key(&id) {
            return id;
        }
        self.entries.insert(id, Entry { id, expr, names: Vec::new() });
        id
    }

    pub fn add_name(&mut self, id: Id, name: String) {
        self.entries.get_mut(&id).unwrap().names.push(name);
    }

    pub fn get(&self, id: Id) -> &Entry {
        self.entries.get(&id).unwrap()
    }
}
