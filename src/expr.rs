use sha3_const::Sha3_224;

use crate::*;

pub type Id = [u8; 28];

#[derive(Default,Debug,Clone,Eq,PartialEq,Ord,PartialOrd,Hash)]
pub struct Expr {
    pub tokens: Vec<Token>,
    pub args: u16,
    pub outputs: u16,
}

impl Expr {
    pub fn id(&self) -> Id {
        let tokens = postcard::to_stdvec(&self.tokens).unwrap();
        Sha3_224::new().update(&tokens).finalize()
    }
    
    pub fn push(mut self, token: Token, db: &Db) -> Result<Self, Self> {
        if self.tokens.len() + 1 >= u16::max_value() as usize {
            return Err(self);
        }
        if self.outputs < token.inputs(db) {
            return Err(self);
        }
        if let Arg(n) = token {
            self.args = self.args.max(n + 1);
        }
        self.outputs -= token.inputs(db);
        self.outputs += token.outputs(db);
        self.tokens.push(token);
        Ok(self)
    }

    pub fn pop(mut self, db: &Db) -> Self {
        if self.tokens.is_empty() { return self; }
        let token = self.tokens.pop().unwrap();
        if self.args > 0 && token == Arg(self.args-1) {
            self.args = self.tokens.iter()
                .filter_map(|t| if let Arg(n) = t { Some(n + 1) } else { None })
                .max().unwrap_or(0);
        }
        self.outputs += token.inputs(db);
        self.outputs -= token.outputs(db);
        self
    }
    
    pub fn slice(&self) -> SubExpr {
        SubExpr(&self.tokens)
    }
    
    pub fn render(&self, db: &Db) -> String {
        let mut r = String::new();
        //let mut arg_num = self.inputs as usize;
        let mut slice = self.slice();
        while !slice.0.is_empty() {
            let t = slice.pop();
            t.render(&mut slice, &mut r, db);
            if !slice.0.is_empty() { r += " ,"; }
        }
        r.chars().rev().collect()
    }
    
    pub fn val(&self, args: &[Value], db: &Db) -> Vec<Value> {
        let mut stack = Vec::new();
        for &token in &self.tokens {
            stack = token.val(args, stack, db);
        }
        stack
    }
}

#[derive(Debug,Copy,Clone,Eq,PartialEq,Ord,PartialOrd,Hash)]
pub struct SubExpr<'a>(&'a [Token]);

impl<'a> SubExpr<'a> {
    pub fn pop(&mut self) -> Token {
        let (new, r) = self.0.split_at(self.0.len()-1);
        self.0 = new;
        r[0]
    }
}
