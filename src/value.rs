use crate::*;
use std::fmt::Write;

#[derive(Clone)]
pub enum Value {
    Integer(i32),
    Expression(Expr),
}
pub use Value::*;

impl Value {
    pub fn render(&self, out: &mut String,  db: &Db) {
        match self {
            Integer(n) => write!(out, "{n}").unwrap(),
            Expression(expr) => *out += &expr.render(db),
        }
    }
}
