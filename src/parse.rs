use crate::*;

#[derive(Debug,Clone,Eq,PartialEq)]
enum SyntaxToken {
    Int(i32),
    Operator(Token),
    LParen,
    RParen,
    Word(String),
    Call(Id),
}

pub fn parse_expr(input: &str, scope: &HashMap<String, Id>, db: &Db) -> Expr {
    let mut token = SyntaxToken::Word(String::new());
    let mut tokens = Vec::new();
    let mut flush = |token| match token {
        SyntaxToken::Word(w) if w.is_empty() => {},
        SyntaxToken::Word(w) if scope.contains_key(&w) => {
            let id = *scope.get(&w).unwrap();
            tokens.push(SyntaxToken::Call(id));
        }
        t => tokens.push(t),
    };
    for c in input.chars() {
        match c {
            '$' => {
                flush(token);
                token = SyntaxToken::Word(c.to_string());
            }
            '0'..='9' => match &mut token {
                SyntaxToken::Int(n) => *n = *n * 10 + c as i32 - '0' as i32,
                SyntaxToken::Word(w) if !w.is_empty() => w.push(c),
                _ => {
                    flush(token);
                    token = SyntaxToken::Int(c as i32 - '0' as i32);
                }
            }
            _ if c.is_alphabetic() => if let SyntaxToken::Word(w) = &mut token {
                w.push(c);
            } else {
                flush(token);
                token = SyntaxToken::Word(c.to_string());
            }
            '-' => {
                flush(token);
                token = SyntaxToken::Operator(Neg);
            }
            '+' => {
                flush(token);
                token = SyntaxToken::Operator(Add);
            }
            '*' => {
                flush(token);
                token = SyntaxToken::Operator(Mul);
            }
            '(' => {
                flush(token);
                token = SyntaxToken::LParen;
            }
            ')' => {
                flush(token);
                token = SyntaxToken::RParen;
            }
            _ => {
                flush(token);
                token = SyntaxToken::Word(String::new());
            }
        }
    }
    flush(token);
    shunting_yard(tokens, db)
}

fn shunting_yard(tokens: Vec<SyntaxToken>, db: &Db) -> Expr {
    let mut r = Expr::default();
    let mut stack = Vec::new();
    
    macro output($token: expr) {
        match $token {
            SyntaxToken::Int(n) => r = r.push(Literal(n), db).unwrap(),
            SyntaxToken::Word(w) => {
                if let Some(n) = parse_arg_num(w) {
                    let t = Arg(n);
                    r = r.push(t, db).unwrap();
                } else {
                    //TODO: arg names
                    let t = Arg(r.args);
                    r = r.push(t, db).unwrap();
                }
            }, 
            SyntaxToken::Operator(op) => {
                while r.outputs < op.inputs(db) {
                    //pad with args
                    let t = Arg(r.args);
                    r = r.push(t, db).unwrap();
                }
                r = r.push(op, db).unwrap();
            },
            SyntaxToken::LParen | SyntaxToken::RParen => {},
            SyntaxToken::Call(id) => {
                let entry = db.get(id);
                while r.outputs < entry.expr.args {
                    //pad with args
                    let t = Arg(r.args);
                    r = r.push(t, db).unwrap();
                }
                r = r.push(Call(id), db).unwrap();
            },
        }
    }
    
    for token in tokens {
        match token {
            SyntaxToken::Operator(op) => {
                while let Some(SyntaxToken::Operator(top_op)) = stack.last() {
                    if op.precedence() > top_op.precedence() {
                        break;
                    }
                    output!(stack.pop().unwrap());
                }
                stack.push(token);
            }
            SyntaxToken::LParen => stack.push(token),
            SyntaxToken::RParen => {
                while let Some(top) = stack.pop() {
                    if top == SyntaxToken::LParen {
                        break;
                    }
                    output!(top);
                }
            },
            _ => output!(token),
        }
    }

    for token in stack.into_iter().rev() {
        output!(token);
    }
    
    r
}

fn parse_arg_num(mut w: String) -> Option<u16> {
    if w.starts_with('$') {
        w = w.chars().skip(1).collect();
        w.parse().ok()
    } else {
        None
    }
}
