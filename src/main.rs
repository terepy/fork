#![feature(decl_macro,exclusive_range_pattern,strict_provenance_atomic_ptr)]

use std::collections::HashMap;
use std::error::Error;
use std::sync::mpsc::{Sender,Receiver,channel};
use macroquad::miniquad::KeyCode;
use macroquad::prelude::*;
use macroquad::ui::{root_ui,hash};

mod expr;
use expr::*;
mod token;
use token::*;
mod parse;
use parse::parse_expr;
mod node;
use node::*;
mod db;
use db::*;
mod value;
use value::*;

struct HistoryLine {
    raw: String,
    expr: Expr,
    assigned_to: String,
    result: Vec<Value>,
}

struct Repl {
    raw_input: String,
    expr: Expr,
    suggestions: Vec<ScoredExpr>,
    send: Sender<(Expr, HashMap<String, Id>, Db, Sender<ScoredExpr>)>,
    recv: Receiver<ScoredExpr>,
    history: Vec<HistoryLine>,
    db: Db,
    scope: HashMap<String, Id>,
}

impl Repl {
    fn new() -> Result<Self, Box<dyn Error>> {
        let expr = Expr::default();
        let send = start_search_loop();
        let (suggestion_send, recv) = channel();
        send.send((expr.clone(), HashMap::new(), Db::default(), suggestion_send))?;

        Ok(Self {
            raw_input: String::new(),
            expr,
            suggestions: Vec::new(),
            send,
            recv,
            history: Vec::new(),
            db: Db::default(),
            scope: HashMap::new(),
        })
    }
    
    fn render(&mut self) -> Result<(), Box<dyn Error>> {
        if is_key_pressed(KeyCode::Enter) {
            self.push_expr()?;
        }
        if is_key_pressed(KeyCode::Up) && !self.history.is_empty() {
            self.raw_input = self.history.last().unwrap().raw.clone();
            self.update_cmd_line(self.history.last().unwrap().expr.clone())?;
        }
        
        self.draw_suggestions()?;
        self.draw_repl()?;

        let raw_expr = self.raw_input.split("->").next().unwrap_or("");
        let new_expr = parse_expr(raw_expr, &self.scope, &self.db);
        if new_expr != self.expr {
            self.update_cmd_line(new_expr)?;
        }
        
        self.suggestions.extend(self.recv.try_iter());
        self.suggestions.sort_by_key(|o| o.score);

        Ok(())
    }

    fn draw_repl(&mut self) -> Result<(), Box<dyn Error>> {
        let pos = (screen_width() / 2.0, screen_height() / 2.0).into();
        let size = (screen_width() / 2.0, screen_height() / 2.0).into();
        root_ui().window(hash!(), pos, size, |ui| {
            for line in &self.history {
                if !line.assigned_to.is_empty() {
                    ui.label(None, &format!("{} -> {}",line.expr.render(&self.db),line.assigned_to));
                } else {
                    ui.label(None, &line.expr.render(&self.db));
                }
                if !line.result.is_empty() {
                    ui.label(None, &format!("= {}",format_seq(&line.result, &self.db)));
                }
            }
            let id = hash!();
            ui.set_input_focus(id);
            ui.input_text(id, "", &mut self.raw_input);
            let inputs = self.expr.args;
            let args: Vec<Value> = (1..=(inputs as i32)).map(Integer).collect();
            let output = self.expr.val(&args, &self.db);
            let expr_output = if inputs == 0 {
                format!("{}", format_seq(&output, &self.db))
            } else {
                format!("{} foo = {}", format_seq(&args, &self.db), format_seq(&output, &self.db))
            };
            ui.label(None, &expr_output);
        });

        Ok(())
    }

    fn draw_suggestions(&mut self) -> Result<(), Box<dyn Error>> {
        let mut result = Ok(());
        let pos = (0.0, screen_height() / 2.0).into();
        let size = (screen_width() / 2.0, screen_height() / 2.0).into();
        root_ui().window(hash!(), pos, size, |ui| {
            let mut keys = [KeyCode::F1, KeyCode::F2, KeyCode::F3, KeyCode::F4, KeyCode::F5, KeyCode::F6, KeyCode::F7, KeyCode::F8, KeyCode::F9, KeyCode::F10, KeyCode::F11, KeyCode::F12].into_iter();
            for ScoredExpr{ score, expr } in self.suggestions.iter().take(20) {
                if expr == &self.expr { continue; }
                if ui.button(None, format!("{} ({})",expr.render(&self.db),score)) {
                    self.expr = expr.clone();
                    result = self.push_expr();
                    break;
                }
                if let Some(key) = keys.next() {
                    ui.same_line(0.0);
                    ui.label(None, &format!("{:?}",key));
                    if is_key_pressed(key) {
                        self.expr = expr.clone();
                        result = self.push_expr();
                        break;
                    }
                }
            }
        });
        result
    }

    fn push_expr(&mut self) -> Result<(), Box<dyn Error>> {
        if self.raw_input.starts_with("delete ") {
            let name = &self.raw_input[7..];
            if let Some(_) = self.scope.remove(name) {
                //TODO
            } else {
                println!("nothing named: {name}");
            }
        }
        let result = if self.expr.args == 0 {
            self.expr.val(&[], &self.db)
        } else {
            Vec::new()
        };
        let name;
        if let Some((_expr, n)) = self.raw_input.split_once("->") {
            name = n.trim().to_string();
        } else {
            name = String::new();
        }
        match (name.is_empty(), self.expr.tokens.is_empty()) {
            (true, true) => {},
            (false, true) => {
                let id = self.db.add(self.history.last().unwrap().expr.clone());
                self.scope.insert(name.to_string(), id);
                self.db.add_name(id, name.to_string());
                self.history.last_mut().unwrap().assigned_to = name;
            }
            (named, false) => {
                if named {
                    let id = self.db.add(self.expr.clone());
                    self.scope.insert(name.to_string(), id);
                    self.db.add_name(id, name.to_string());
                }
                self.history.push(HistoryLine {
                    raw: self.raw_input.clone(),
                    expr: self.expr.clone(),
                    result,
                    assigned_to: name,
                });
            },
        }
        self.raw_input = String::new();
        self.update_cmd_line(Expr::default()) 
    }

    fn update_cmd_line(&mut self, expr: Expr) -> Result<(), Box<dyn Error>> {
        self.expr = expr;
        let (send, recv) = channel();
        self.recv = recv;
        self.send.send((self.expr.clone(), self.scope.clone(), self.db.clone(), send))?;
        self.suggestions.clear();
        Ok(())
    }
}

fn main() {
    macroquad::Window::new("fork", async {
        if let Err(err) = amain().await {
            macroquad::logging::error!("Error: {:?}", err);
        }
    })
}
async fn amain() -> Result<(), Box<dyn Error>> {
    let mut repl = Repl::new()?;

    loop {
        if is_key_pressed(KeyCode::Escape) {
            break Ok(());
        }

        clear_background(Color::from_hex(0x666666));

        repl.render()?;

        next_frame().await;
    }
}

/*fn arg_name(i: usize) -> String {
    ('a'..).nth(i).unwrap().to_string()
}*/

fn format_seq(seq: &[Value], db: &Db) -> String {
    let mut r = String::new();
    for i in 0..seq.len() {
        if i != 0 {
            r += ", ";
        }
        seq[i].render(&mut r, db);
    }
    r
}
